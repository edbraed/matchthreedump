﻿using System;
using System.Drawing;

namespace MatchThree.Model
{
//===========================================================================
//= Bonuses =================================================================
//===========================================================================
    
    public class IBonus
    {
        protected Point m_pos = new Point(-1, -1);
        private Point m_dir = new Point(0, 0);
        private int m_healt = -1;
        private DateTime m_timestamp = DateTime.Now;
        private double m_delay = -1.0;

        public IBonus(Point pos, Point dir, int healt, double delay = -1.0)
        {
            m_pos = pos;
            m_dir = dir;
            m_healt = healt;
            m_delay = delay;
        }
        public Point Pos { get { return m_pos; } }
        public int Healt { get { return m_healt; } }

        public virtual String resPath() { return ""; }

        public bool isAlive()
        {
            if (m_delay < 0.0f)
                return true;
            TimeSpan lastTime = (TimeSpan)(DateTime.Now - m_timestamp);
            return lastTime.TotalMilliseconds >= m_delay;
        }

        public virtual void update(FieldModel field)
        {
            m_pos.X += m_dir.X;
            m_pos.Y += m_dir.Y;
            if (m_healt > 0)
                m_healt--;
        }
    }

    public class LineDestroyer : IBonus
    {
        public LineDestroyer(Point pos, Point dir) : base(pos, dir, -1)
        { }

        public override String resPath() { return "fireball"; }

        public override void update(FieldModel field)
        {
            field.explodeStone(m_pos);
            base.update(field);
        }
    }
    public class BombDestroyer : IBonus
    {
        public BombDestroyer(Point pos) : base(pos, new Point(0, 0), 1, 250.0)
        { }

        public override String resPath() { return "fireball2"; }

        public override void update(FieldModel field)
        {
            field.explodeStone(m_pos);
            base.update(field);
        }
    }

//===========================================================================
//= Buffs ===================================================================
//===========================================================================

    public class IBuff
    {
        public virtual String resPath() { return ""; }
        public virtual IBonus[] getBonuses(Point pos) { return null; }
    }

    public class VLineBuff : IBuff
    {
        public override String resPath() { return "v-line"; }
        public override IBonus[] getBonuses(Point pos)
        {
            return new IBonus[]{ new LineDestroyer(new Point(pos.X, pos.Y + 1), new Point(0,  1)),
                                 new LineDestroyer(new Point(pos.X, pos.Y - 1), new Point(0, -1)) };
        }
    }

    public class HLineBuff : IBuff
    {
        public override String resPath() { return "h-line"; }
        public override IBonus[] getBonuses(Point pos)
        {
            return new IBonus[]{ new LineDestroyer(new Point(pos.X + 1, pos.Y), new Point( 1, 0)),
                                 new LineDestroyer(new Point(pos.X - 1, pos.Y), new Point(-1, 0)) };
        }
    }

    public class BombBuff : IBuff
    {
        public override String resPath() { return "bomb"; }
        public override IBonus[] getBonuses(Point pos)
        {
            return new IBonus[]{ new BombDestroyer(new Point(pos.X - 1, pos.Y - 1)),
                                 new BombDestroyer(new Point(pos.X,     pos.Y - 1)),
                                 new BombDestroyer(new Point(pos.X + 1, pos.Y - 1)),
                                 new BombDestroyer(new Point(pos.X - 1, pos.Y    )),
                                 new BombDestroyer(new Point(pos.X + 1, pos.Y    )),
                                 new BombDestroyer(new Point(pos.X - 1, pos.Y + 1)),
                                 new BombDestroyer(new Point(pos.X,     pos.Y + 1)),
                                 new BombDestroyer(new Point(pos.X + 1, pos.Y + 1)) };
        }
    }

//===========================================================================
//= Stones ==================================================================
//===========================================================================

    public class IStone : ICloneable
    {
        public IBuff buff = null;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        public virtual String resPath() { return ""; }

        public IBonus[] getBonuses(Point pos)
        {
            if (buff != null)
                return buff.getBonuses(pos);
            return null;
        }
    }

    public class Ruby : IStone
    {
        public override String resPath()  { return "ruby"; }
    }

    public class Sapphire : IStone
    {
        public override String resPath() { return "sapphire"; }
    }

    public class Emerald : IStone
    {
        public override String resPath() { return "emerald"; }
    }

    public class Topaz : IStone
    {
        public override String resPath() { return "topaz"; }
    }

    public class Alexandrite : IStone
    {
        public override String resPath() { return "alexandrite"; }
    }

    public class Amethyst : IStone
    {
        public override String resPath() { return "amethyst"; }
    }

    class StoneRegistry
    {
        public static IStone[] stonesTypes = { new Ruby(), new Sapphire(), new Emerald(), 
                                               new Topaz(), new Alexandrite()/*, new Amethyst()*/ };
        public static IBuff[] buffsTypes  = { new VLineBuff(), new HLineBuff(), new BombBuff() };
        public static IBonus[] bonusTypes = { new LineDestroyer(new Point(), new Point()),
                                              new BombDestroyer(new Point())};
        static Random rnd = new Random();

        public static IStone CreateRndStone()
        {
            return (IStone)stonesTypes[rnd.Next(0, stonesTypes.Length)].Clone();
        }
    }
}
