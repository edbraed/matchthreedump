﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MatchThree.Model
{
    public enum FieldStep
    {
        L = 0, // Left move
        R = 1, // Right move
        U = 2, // Up move
        D = 3  // Down move
    }

    enum LineType
    {
        V = 0, // Vertical
        H = 1  // Horizontal
    }

    public enum PolyominoType
    {
        L = 0, // Line
        C = 1  // Cross
    }

    class Triplet
    {
        //public Dictionary<IStone, Point> map = new Dictionary<IStone, Point>();
        public HashSet<Point> set = new HashSet<Point>();
        public LineType t = LineType.V;
    }

    class Polyomino
    {
        //public Dictionary<IStone, Point> map = new Dictionary<IStone, Point>();
        public HashSet<Point> set = new HashSet<Point>();
        public Point cntr = new Point(-1, -1);
        public PolyominoType t = PolyominoType.L;
        public LineType tl = LineType.V;
    }

    public class FieldModel
    {
        private IStone[,] m_stones = null;
        private List<IBonus> m_bonuses = new List<IBonus>();
        private Size m_size = new Size(0, 0);
        private uint m_score = 0;
        Point[] badMove = null;
        private Random rndGen = new Random();


        public FieldModel(Size size)
        {
            m_size = size;
            m_stones = new IStone[m_size.Width, m_size.Height];
            for (uint i = 0; i < m_stones.GetLength(0); i++)
            {
                for (uint j = 0; j < m_stones.GetLength(1); j++)
                    m_stones[i, j] = null;
            }
        }

//===========================================================================

        public int Width { get { return m_size.Width; } }
        public int Height { get { return m_size.Height; } }
        public uint Score { get { return m_score; } }

        public IStone[,] Stones { get { return m_stones; } }
        public List<IBonus> Bonuses { get { return m_bonuses; } }

//===========================================================================

        private IStone getStone(Point pt)
        {
            if (pt.X > -1 && pt.X < Width)
            {
                if (pt.Y > -1 && pt.Y < Height) 
                    return this.m_stones[pt.X, pt.Y];
            }
            return null;
        }

        private IStone getStone(int x, int y)
        {
            return getStone(new Point(x,y));
        }

//===========================================================================

        private bool checkMoves(Point pt)
        {
            bool checkLine(IStone[] ln)
            {
                for (var x = 1; x < ln.Length - 1; x++)
                {
                    var stn0 = ln[x - 1];
                    var stn1 = ln[x];
                    var stn2 = ln[x + 1];

                    if (stn0 == null || stn1 == null || stn2 == null) 
                        continue;

                    if (stn0.GetType() == stn1.GetType() && stn2.GetType() == stn1.GetType()) 
                        return true;
                }
                return false;
            }

            var s = this.getStone(pt);

            var s0 = this.getStone(pt.X - 2, pt.Y);
            var s1 = this.getStone(pt.X - 1, pt.Y);
            var s2 = this.getStone(pt.X + 1, pt.Y);
            var s3 = this.getStone(pt.X + 2, pt.Y);
            var s4 = this.getStone(pt.X, pt.Y - 2);
            var s5 = this.getStone(pt.X, pt.Y - 1);
            var s6 = this.getStone(pt.X, pt.Y + 1);
            var s7 = this.getStone(pt.X, pt.Y + 2);
            var ln1 = new IStone[] { s0, s1, s, s2, s3 };
            var ln2 = new IStone[] { s4, s5, s, s6, s7 };
            if (checkLine(ln1) || checkLine(ln2)) 
                return true;
            return false;
        }

//===========================================================================

        public void move(Point pt, FieldStep s)
        {
            Point a = pt;
            Point b = new Point(-1, -1);

            if (s == FieldStep.L && a.X > 0) 
                b = new Point(a.X - 1, a.Y);
            else if (s == FieldStep.R && a.X< Width - 1) 
                b = new Point(a.X + 1, a.Y);
            else if (s == FieldStep.U && a.Y > 0) 
                b = new Point(a.X, a.Y - 1);
            else if (s == FieldStep.D && a.Y < Height - 1) 
                b = new Point(a.X, a.Y + 1);

            if (b.X < 0 || b.Y < 0) 
                return;

            IStone sa = this.getStone(a);
            IStone sb = this.getStone(b);
            if (sa == null || sb == null) 
                return;

            (m_stones[a.X, a.Y], m_stones[b.X, b.Y]) = (m_stones[b.X, b.Y], m_stones[a.X, a.Y]);

            if (!this.checkMoves(a) && !this.checkMoves(b))
                badMove = new Point[] { a, b };
        }

//===========================================================================

        public bool updateBonuses()
        {
            for(var i = m_bonuses.Count - 1; i > -1; i--)
            {
                var b = m_bonuses[i];
                if (b.isAlive())
                    b.update(this);
                if (b.Pos.X < 0 || b.Pos.Y < 0 || b.Pos.X >= Width || b.Pos.Y >= Height || b.Healt == 0)
                    m_bonuses.RemoveAt(i);
            }
            return m_bonuses.Count > 0;
        }

//===========================================================================

        public bool falls()
        {
            bool bFalls = false;
            for (var x = 0; x < Width; x++)
            {
                bool cells = false;
                uint maxEmpty = 0;
                for (uint y = 0; y < Height; y++)
                {
                    if (m_stones[x, y] == null) 
                        maxEmpty = y;
                    else 
                        cells = true;
                }
                if (!cells && maxEmpty != Height - 1) 
                    continue;

                for (uint i = maxEmpty; i > 0; i--)
                {
                    (m_stones[x, i], m_stones[x, i - 1]) = (m_stones[x, i - 1], m_stones[x, i]);
                    bFalls = true;
                }

                if (m_stones[x, 0] == null && m_stones[x, 1] == null)
                {
                    m_stones[x, 0] = StoneRegistry.CreateRndStone();
                    bFalls = true;
                }
            }
            return bFalls;
        }

//===========================================================================

        public void explodeStone(Point pt)
        {
            if (getStone(pt) != null)
            {
                m_score++;
                IBonus[] bonuses = m_stones[pt.X, pt.Y].getBonuses(pt);
                if (bonuses != null)
                    m_bonuses.AddRange(bonuses);
                m_stones[pt.X, pt.Y] = null;
            }
        }

//===========================================================================

        private Triplet findTriple(int x, int y)
        {
            var s0 = this.getStone(x, y);
            if (s0 == null)
                return null;

            var s1 = getStone(x - 1, y);
            var s2 = getStone(x + 1, y);
            var s3 = getStone(x, y - 1);
            var s4 = getStone(x, y + 1);

            var s = new Triplet();

            if (s1 != null && s2 != null && s0.GetType() == s1.GetType() &&
                                            s0.GetType() == s2.GetType())
            {
                s.set.Add(new Point(x, y));
                s.set.Add(new Point(x - 1, y));
                s.set.Add(new Point(x + 1, y));
                s.t = LineType.H;
                return s;
            }
            else if (s3 != null && s4 != null && s0.GetType() == s3.GetType() &&
                                                 s0.GetType() == s4.GetType())
            {
                s.set.Add(new Point(x, y));
                s.set.Add(new Point(x, y - 1));
                s.set.Add(new Point(x, y + 1));
                s.t = LineType.V;
                return s;
            }

            return null;
        }

//===========================================================================
        private void ConcatSets(ref HashSet<Point> set1, HashSet<Point> set2)
        {
            foreach (Point k in set2)
            {
                if (!set1.Contains(k))
                    set1.Add(k);
            }
        }

//===========================================================================
        public bool explodes()
        {
            if (badMove != null)
            {
                var a = badMove[0];
                var b = badMove[1];
                (m_stones[a.X, a.Y], m_stones[b.X, b.Y]) = (m_stones[b.X, b.Y], m_stones[a.X, a.Y]);
                badMove = null;
                return false;
            }

            List<Triplet> triplets = new List<Triplet>();
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    Triplet triplet = findTriple(i, j);
                    if (triplet != null)
                        triplets.Add(triplet);
                }
            }

            if (triplets.Count < 1)
                return false;

            HashSet<IStone> m_stonesSet = new HashSet<IStone>();
            List<Polyomino> polyominos = new List<Polyomino>();
            for (int i = 0; i < triplets.Count; i++)
            {
                Polyomino polyomino = new Polyomino();
                polyomino.set = triplets[i].set;
                polyomino.cntr = triplets[i].set.First();
                polyomino.tl = triplets[i].t;
                polyomino.t = PolyominoType.L;

                for (int j = 0; j < triplets.Count; j++)
                {
                    if (i == j)
                        continue;

                    Point tripletsIsec = new Point(-1, -1);

                    foreach (Point k in triplets[i].set)
                    {
                        if (triplets[j].set.Contains(k))
                        {
                            tripletsIsec = k;
                            break;
                        }
                    }

                    if (tripletsIsec.X > -1 && tripletsIsec.Y > -1)
                    {
                        if (triplets[j].t != triplets[i].t)
                        {
                            polyomino.t = PolyominoType.C;
                            polyomino.cntr = tripletsIsec;
                        }
                        ConcatSets(ref polyomino.set, triplets[j].set);
                    }
                }

                bool uniqForm = true;
                foreach(Point k in polyomino.set)
                {
                    if(m_stonesSet.Contains(m_stones[k.X, k.Y]))
                        uniqForm = false;
                }
        
                if(uniqForm)
                {
                    polyominos.Add(polyomino);
                    foreach (Point k in polyomino.set)
                        m_stonesSet.Add(m_stones[k.X, k.Y]);
                }
            }

            for (int i = 0; i < polyominos.Count; i++)
            {

                IStone newStone = null;
                if (polyominos[i].set.Count > 3)
                {
                    Point pt = polyominos[i].set.First();
                    newStone = (IStone)m_stones[pt.X, pt.Y].Clone();

                    if (polyominos[i].t == PolyominoType.L && polyominos[i].set.Count == 4)
                    {
                        if (polyominos[i].tl == LineType.V)
                            newStone.buff = new VLineBuff();
                        else
                            newStone.buff = new HLineBuff();
                    }
                    else
                        newStone.buff = new BombBuff();
                }

                foreach (Point k in polyominos[i].set)
                    explodeStone(k);

                if(newStone != null)
                    Stones[polyominos[i].cntr.X, polyominos[i].cntr.Y] = newStone;
            }
            return true;
        }

    }
}
