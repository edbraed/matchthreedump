﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using MatchThree.Model;

namespace MatchThree.View
{
    public partial class MatchThreeView : UserControl
    {
        private FieldModel m_field = null;
        private FieldView m_fieldView = null;

        private enum LogicStage
        {
            Falls = 0,
            Explodes = 1,
            Final = 2
        }

        private int m_fvCellSz = 0;
        private PointF m_fvBeg = new PointF(0, 0);

        private bool m_enableLogic = true;
        private long m_logicDelay = 0;
        private LogicStage m_logicStage = LogicStage.Explodes;
        private int m_gameoverTime = 0;
        private uint m_lastScore = 0;

        Thread mainLoopThread = null;
        Stopwatch swDeltatime = new Stopwatch();
        Stopwatch swLogicUpdate = new Stopwatch();
        Stopwatch swBonusesUpdate = new Stopwatch();

        float m_deltatime = 0.016f;

//===========================================================================

        public MatchThreeView(Size fieldSize, int gameoverTime)
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            
            m_field = new FieldModel(fieldSize);
            m_fieldView = new FieldView(m_field);
            m_gameoverTime = gameoverTime;
            lblTimer.Text = "Time: " + m_gameoverTime.ToString();
            m_lastScore = 0;

            mainLoopThread = new Thread(new ThreadStart(this.mainLoop));
            mainLoopThread.Start();

            swLogicUpdate.Start();
            swDeltatime.Start();
            swBonusesUpdate.Start();

            resetFieldView();
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            mainLoopThread.Abort();
            base.OnHandleDestroyed(e);
        }

//===========================================================================

        private void mainLoop()
        {
            while (true)
            {
                swDeltatime.Restart();
                if (m_enableLogic && swLogicUpdate.ElapsedMilliseconds >= m_logicDelay)
                {
                    m_logicDelay = 50;
                    swLogicUpdate.Restart();

                    if (m_logicStage == LogicStage.Explodes && !m_field.explodes())
                        m_logicStage = LogicStage.Falls;
                    else if (m_logicStage == LogicStage.Falls && !m_field.falls())
                    {
                        if (!m_field.explodes())
                            m_logicStage = LogicStage.Final;
                        else
                            m_logicStage = LogicStage.Explodes;
                    }
                    else if (m_logicStage == LogicStage.Final)
                    {
                        m_enableLogic = false;
                        m_logicStage = LogicStage.Explodes;
                    }

                    if (m_lastScore != m_field.Score)
                    {
                        m_lastScore = m_field.Score;
                        pbFieldView.Invoke((MethodInvoker)delegate
                        { 
                            lblScore.Text = "Score: " + m_lastScore.ToString();
                            lblScore.Refresh();
                        });
                    }
                }

                if (swBonusesUpdate.ElapsedMilliseconds >= 50)
                {
                    m_enableLogic = !m_field.updateBonuses();
                    swBonusesUpdate.Restart();
                }

                pbFieldView.Invoke((MethodInvoker)delegate { pbFieldView.Refresh(); });
                swDeltatime.Stop();
                m_deltatime = (float)swDeltatime.ElapsedMilliseconds / 1000.0f;
                //Thread.Sleep(1);
                Thread.Sleep(TimeSpan.FromTicks(100));
            }
        }

        private void pbFieldView_Resize(object sender, EventArgs e)
        {
            resetFieldView();
        }

//===========================================================================

        private void resetFieldView()
        {
            int fw = m_field.Width;
            int fh = m_field.Height;
            m_fvCellSz = (Math.Min(pbFieldView.Width, pbFieldView.Height) - 100) / Math.Max(fw, fh);

            m_fvBeg.X = (pbFieldView.Width - m_fvCellSz * fw - m_fvCellSz / 2) / 2;
            m_fvBeg.Y = (pbFieldView.Height - m_fvCellSz * fh - m_fvCellSz / 2) / 2;

            m_fieldView.Resize(m_fvBeg, m_fvCellSz);
        }

        private void addLogicDelay(long d)
        {
            m_logicDelay += d;
        }

//===========================================================================

        private void pbFieldView_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            m_fieldView.draw(ref g, m_deltatime);
        }

//===========================================================================

        private void pbFieldView_Click(object sender, EventArgs e)
        {
            var me = e as MouseEventArgs;
            if (me.Button != MouseButtons.Left)
                return;

            PointF maxpt = new PointF(pbFieldView.Width - m_fvBeg.X,
                                      pbFieldView.Height - m_fvBeg.Y);
            if (me.X < m_fvBeg.X || me.Y < m_fvBeg.Y ||
                me.X > maxpt.X   || me.Y > maxpt.Y)
            {
                m_fieldView.SelectOff();
                return;
            }

            Point GetCell()
            {
                var locPos = new PointF(me.X - m_fvBeg.X,
                                        me.Y - m_fvBeg.Y);
                Point fsz = new Point((int)m_field.Width, (int)m_field.Height);
                PointF normPos = new PointF(locPos.X / (float)(m_fvCellSz * fsz.X),
                                            locPos.Y / (float)(m_fvCellSz * fsz.Y));
                return new Point((int)(normPos.X * fsz.X),
                                 (int)(normPos.Y * fsz.Y));
            }

            if (!m_fieldView.Select)
                m_fieldView.SelectOn(GetCell());
            else
            {
                Point selectCell1 = m_fieldView.SelectCell;
                Point selectCell2 = GetCell();
                Point normCell = new Point(Math.Abs(selectCell2.X - selectCell1.X),
                                           Math.Abs(selectCell2.Y - selectCell1.Y));
                if (normCell.X == 1 && normCell.Y == 0 || normCell.Y == 1 && normCell.X == 0)
                {
                    FieldStep st = FieldStep.L;
                    if (selectCell2.X < selectCell1.X)
                        st = FieldStep.L;
                    else if (selectCell2.X > selectCell1.X)
                        st = FieldStep.R;
                    else if (selectCell2.Y < selectCell1.Y)
                        st = FieldStep.U;
                    else if (selectCell2.Y > selectCell1.Y)
                        st = FieldStep.D;

                    m_field.move(selectCell1, st);
                    m_enableLogic = true;
                    addLogicDelay(200);
                }
                m_fieldView.SelectOff();
            }
        }

//===========================================================================

        private void gameoverTimer_Tick(object sender, EventArgs e)
        {
            m_gameoverTime--;
            lblTimer.Text = "Time: " + m_gameoverTime.ToString();
            if (m_gameoverTime < 1)
            {
                gameoverTimer.Stop();
                mainLoopThread.Abort();
                pbFieldView.Hide();
                MainMenu mm = (MainMenu)(this.Parent);
                mm.GameOver(this.m_lastScore);
            }
        }
    }
}
