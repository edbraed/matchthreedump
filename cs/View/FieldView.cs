﻿using MatchThree.Model;
using MatchThree.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Resources;

namespace MatchThree.View
{
    public class FieldView
    {
        private ResourceManager m_rmgr = Resources.ResourceManager;
        private Dictionary<int, Image> m_stoneImgs = new Dictionary<int, Image>();
        private Dictionary<int, Image> m_buffImgs = new Dictionary<int, Image>();
        private Dictionary<int, Image> m_bonusesImgs = new Dictionary<int, Image>();
        private Dictionary<int, Image> m_fieldImgs = new Dictionary<int, Image>();

        private Dictionary<IStone, PointF> m_visStones = new Dictionary<IStone, PointF>();
        private Dictionary<IBonus, PointF> m_visBonuses = new Dictionary<IBonus, PointF>();

        private FieldModel m_field = null;
        private int m_fvCellSz = 0;
        private PointF m_fvBeg = new Point();

        bool m_selectEnable = false;
        Point m_selectCell = new Point(-1, -1);

        public FieldView(FieldModel field)
        {
            m_field = field;

            // load stones resources
            for (int i = 0; i < StoneRegistry.stonesTypes.Length; i++)
            {
                Image sImg = (Image)m_rmgr.GetObject(StoneRegistry.stonesTypes[i].resPath());
                m_stoneImgs.Add(StoneRegistry.stonesTypes[i].GetType().GetHashCode(), sImg);
            }

            // load buffs resources
            for (int i = 0; i < StoneRegistry.buffsTypes.Length; i++)
            {
                Image bImg = (Image)m_rmgr.GetObject(StoneRegistry.buffsTypes[i].resPath());
                m_buffImgs.Add(StoneRegistry.buffsTypes[i].GetType().GetHashCode(), bImg);
            }

            // load bonuses resources
            for (int i = 0; i < StoneRegistry.bonusTypes.Length; i++)
            {
                Image bImg = (Image)m_rmgr.GetObject(StoneRegistry.bonusTypes[i].resPath());
                m_bonusesImgs.Add(StoneRegistry.bonusTypes[i].GetType().GetHashCode(), bImg);
            }

            // load other resources
            m_fieldImgs.Add(0, (Image)m_rmgr.GetObject("cell_select0"));
            m_fieldImgs.Add(1, (Image)m_rmgr.GetObject("cell_select1"));
        }

//===========================================================================

        public void Resize(PointF fvBeg, int fvCellSz)
        {
            m_fvBeg = fvBeg;
            m_fvCellSz = fvCellSz;
        }

        public void SelectOn(Point pt)
        {
            m_selectCell = pt;
            m_selectEnable = true;
        }

        public void SelectOff()
        {
            m_selectEnable = false;
        }

        public bool Select { get { return m_selectEnable; } }
        public Point SelectCell { get { return m_selectCell; } }

//===========================================================================

        private void ClearVisModel<T>(ref Dictionary<T, PointF> map, HashSet<T> whiteset)
        {
            List<T> rKeys = new List<T>();
            foreach (KeyValuePair<T, PointF> kv in map)
            {
                if (!whiteset.Contains(kv.Key))
                    rKeys.Add(kv.Key);
            }
            foreach (T k in rKeys)
                map.Remove(k);
        }

//===========================================================================

        public void draw(ref Graphics g, float deltatime)
        {
            PointF Normalize(PointF v)
            {
                float len = (float)Math.Sqrt(v.X * v.X + v.Y * v.Y);
                if (len < float.Epsilon)
                    return new PointF(0.0f, 0.0f);
                float invL = 1.0f / len;
                return new PointF(v.X * invL, v.Y * invL);
            }

            void updateVizModel<T>(ref Dictionary<T, PointF> dict, T obj, PointF ptB)
            {
                if (dict.ContainsKey(obj))
                {
                    var ptA = dict[obj];
                    PointF dir = Normalize(new PointF(ptB.X - ptA.X, ptB.Y - ptA.Y));
                    dir.X *= Math.Min(deltatime * 10.0f, 1.0f);
                    dir.Y *= Math.Min(deltatime * 10.0f, 1.0f);
                    var np = new PointF(ptA.X + dir.X, ptA.Y + dir.Y);

                    dict[obj] = new PointF(np.X > ptA.X ? Math.Min(np.X, ptB.X) : Math.Max(np.X, ptB.X),
                                           np.Y > ptA.Y ? Math.Min(np.Y, ptB.Y) : Math.Max(np.Y, ptB.Y));
                }
                else
                {
                    if(ptB.X >= 0 && ptB.X < m_field.Width && ptB.Y >= 0 && ptB.Y < m_field.Height)
                        dict.Add(obj, ptB);
                }
            }

            PointF getCellCoord(PointF cell)
            {
                return new PointF(m_fvCellSz * cell.X + m_fvBeg.X,
                                  m_fvCellSz * cell.Y + m_fvBeg.Y);
            }

            // update visual models
            HashSet<IStone> eStones = new HashSet<IStone>(); // existing stones
            for (uint x = 0; x < m_field.Width; x++)
            {
                for (uint y = 0; y < m_field.Height; y++)
                {
                    var stone = m_field.Stones[x, y];
                    if (stone == null)
                        continue;
                    updateVizModel(ref m_visStones, stone, new PointF(x, y));
                    eStones.Add(stone);
                }
            }

            HashSet<IBonus> eBonuses = new HashSet<IBonus>(); // existing bonuses
            foreach (IBonus bonus in m_field.Bonuses)
            {
                updateVizModel(ref m_visBonuses, bonus, bonus.Pos);
                eBonuses.Add(bonus);
            }

            // render visual models
            foreach (KeyValuePair<IStone, PointF> kv in m_visStones)
            {
                Image sImg = m_stoneImgs[kv.Key.GetType().GetHashCode()];
                PointF fp = getCellCoord(kv.Value);
                g.DrawImage(sImg, fp.X, fp.Y, m_fvCellSz, m_fvCellSz);
                if (kv.Key.buff != null)
                {
                    Image bImg = m_buffImgs[kv.Key.buff.GetType().GetHashCode()];
                    g.DrawImage(bImg, fp.X, fp.Y, m_fvCellSz, m_fvCellSz);
                }
            }

            foreach (KeyValuePair<IBonus, PointF> kv in m_visBonuses)
            {
                PointF fp = getCellCoord(kv.Value);
                Image bImg = m_bonusesImgs[kv.Key.GetType().GetHashCode()];
                g.DrawImage(bImg, fp.X, fp.Y, m_fvCellSz, m_fvCellSz);
            }

            // Clear visual models
            ClearVisModel(ref m_visStones, eStones);
            ClearVisModel(ref m_visBonuses, eBonuses);

            // render select cell
            if (m_selectEnable)
            {
                void drawSelectCell(ref Graphics gr, int imgId, int x, int y)
                {
                    if (x < 0 || y <= 0 || x >= m_field.Width || y >= m_field.Height)
                        return;
                    PointF fp = getCellCoord(new PointF(x, y));
                    gr.DrawImage(m_fieldImgs[imgId], fp.X, fp.Y, m_fvCellSz, m_fvCellSz);
                }
                drawSelectCell(ref g, 0, m_selectCell.X, m_selectCell.Y);
                drawSelectCell(ref g, 1, m_selectCell.X + 1, m_selectCell.Y);
                drawSelectCell(ref g, 1, m_selectCell.X - 1, m_selectCell.Y);
                drawSelectCell(ref g, 1, m_selectCell.X, m_selectCell.Y + 1);
                drawSelectCell(ref g, 1, m_selectCell.X, m_selectCell.Y - 1);
            }
        }
    }
}
