﻿using System;
using System.Drawing;
using System.Windows.Forms;

using MatchThree.View;

namespace MatchThree
{
    public partial class MainMenu : Form
    {
        MatchThreeView match3View = null;

        public MainMenu()
        {
            InitializeComponent();
            CenterMainMenu();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            mainMenuPanel.Hide();
            match3View = new MatchThreeView(new Size(8, 9), 60);
            match3View.Parent = this;
            match3View.Dock = DockStyle.Fill;
            match3View.Show();
        }

        public void GameOver(uint score)
        {
            mainMenuPanel.Show();
            match3View.Hide();
            match3View = null;
            MessageBox.Show("Time's up, your score: " + score.ToString(), "Game Over!");
        }

        private void CenterMainMenu()
        {
            mainMenuPanel.Location = new Point(Width / 2 - mainMenuPanel.Width / 2,
                                               Height / 2 - mainMenuPanel.Height / 2);
        }

        private void MainMenu_Resize(object sender, EventArgs e)
        {
            CenterMainMenu();
        }
    }
}
