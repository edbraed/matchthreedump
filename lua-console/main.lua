#!/usr/bin/env lua

require ".Stone";
require ".Field";
--[[
	Разделение на Модель и Визуализацию у меня реализовано просто использованием dump()
	в качестве визуализатора.
	Т.е. по сути единственное что требуется переписать под полноценный рендер
	- это функция dump

	Реализация камней в виде иерархии наследования подразумевает возможность
	определения некого метода, который бы обрабатывал событие уничтожения конкретного
	камня.
	Пробросив ему экземпляр игрового поля, можно реализовать любое нужное нам поведение.
	Для примера, можно реализовать камень, который взрывает всё в неком радиусе.

	Реализация обработки различных комбинаций камней - например ABA или BAB и тд
	Моё видение заключается в определении массива последовательно выполняющихся лямбд внутри tick()
	Например 1ая из них обрабатывала бы классическую комбинацию из 3х одинаковых камней, 
	в тоже время после её выполнения (или невыполнения), выполнялась бы 2ая функция
	обрабатывающая менее тривиальные случаи.
	Тоже поведение конечно можно реализовать и иерархией объектов Executor'ов, 
	в случае необходимости ещё большейй архитектурной расширяемости..
--]]


field = Field:new()
field:init(10, 10)
field:dump()

while true do

	function split(s, sep)
		local fields = {}
		local sep = sep or " "
		local pattern = string.format("([^%s]+)", sep)
		string.gsub(s, pattern, function(c) fields[#fields + 1] = c end)
		return fields
	end

	input = split(io.read(), ' ')

	if input[1] == "exit" then
		break
	elseif input[1] == "m" and #input == 4 then
		sw = tonumber(input[2])
		sh = tonumber(input[3])
		st = input[4]
		if sw and sh and (st == "l" or st == "r" or st == "u" or st == "d") then
			sw = sw + 1
			sh = sh + 1
			print(sw, sh, st)
			field:move(sw, sh, st)

			repeat field:dump() until not field:tick()

			while not field:movesAvaliable() do
				field:mix()
				field:dump()
			end

		else
			print("error command! Expected commands 'exit', 'm <i> <j> <l/r/u/d>'")
		end
	else
		print("error command! Expected commands 'exit', 'm <i> <j> <l/r/u/d>'")
	end
end