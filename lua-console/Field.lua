require ".Stone"

Field = { w = 0, h = 0, bm = false, stones = {} }

--=======================================================

function Field:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

--=======================================================

function Field:init(w, h)
	math.randomseed(0)
	self.w = w
	self.h = h
	for x = 1, self.w do
		self.stones[x] = {}
		for y = 1, self.h do
			self.stones[x][y] = RandomStone()
		end
	end

    -- We immediately run through all possible matches
	while self:tick() do end
end

--=======================================================

function Field:get(x, y)
	if x <= self.w and x >= 1 and y <= self.h and y >= 1 then
		return self.stones[x][y]
	end
	return nil
end

--=======================================================

function Field:move(x, y, s)
	function swap(tbl, x1, y1, x2, y2)
		local tmp = tbl[x1][y1]
		tbl[x1][y1] = tbl[x2][y2]
		tbl[x2][y2] = tmp
	end

	if s == 'l' and x > 1 then
		swap(self.stones, x, y, x - 1, y)
	elseif s == 'r' and x < self.w then
		swap(self.stones, x, y, x + 1, y)
	elseif s == 'u' and y > 1 then
		swap(self.stones, x, y, x, y - 1)
	elseif s == 'd' and y < self.h then
		swap(self.stones, x, y, x, y + 1)
	end
end

--=======================================================

function Field:mix()
	for x = 1, self.w do
		for y = 1, self.h do
			local rnd = math.random(1,4)
			if     rnd == 1 then self:move(x,y, "r")
			elseif rnd == 2 then self:move(x,y, "l")
			elseif rnd == 3 then self:move(x,y, "u")
			elseif rnd == 4 then self:move(x,y, "d")
			end
		end
	end
	self.bm = true
end

--=======================================================

function Field:movesAvaliable()

	function checkLine(verical, i, line)
		
		function checkElement(x, y, t, vertical)
			local sn = self:get(x, y).n
			if t then
				local s1, s2
				if vertical then                  -- [ ][x][ ]
					s1 = self:get(x - 1, y + 1)   -- [?][ ][?]
					s2 = self:get(x + 1, y + 1)   -- [ ][x][ ]
					
				else                              -- [ ][?][ ]
					s1 = self:get(x + 1, y - 1)   -- [x][ ][x]
					s2 = self:get(x + 1, y + 1)   -- [ ][?][ ]
				end
				return (s1 and s1.exist and s1.n == sn) or (s2 and s2.exist and s2.n == sn)
			elseif not t then
				local s1, s2, s3, s4, s5, s6
				if vertical then                    -- [ ][?][ ]
					s1 = self:get(x, y - 2)         -- [?][ ][?]
					s2 = self:get(x, y + 3)         -- [ ][x][ ]
					s3 = self:get(x - 1, y - 1)     -- [ ][x][ ]
					s4 = self:get(x + 1, y - 1)     -- [?][ ][?]
					s5 = self:get(x - 1, y + 2)     -- [ ][?][ ]
					s6 = self:get(x + 1, y + 2)     
				else
					s1 = self:get(x - 2, y)         -- [ ][?][ ][ ][?][ ]
					s2 = self:get(x + 3, y)         -- [?][ ][x][x][ ][?]
					s3 = self:get(x - 1, y - 1)     -- [ ][?][ ][ ][?][ ]
					s4 = self:get(x - 1, y + 1)
					s5 = self:get(x + 2, y - 1)
					s6 = self:get(x + 2, y - 1)
				end
				return (s1 and s1.exist and sn == s1.n) or (s2 and s2.exist and sn == s2.n) or
				       (s3 and s3.exist and sn == s3.n) or (s4 and s4.exist and sn == s4.n) or
				       (s5 and s5.exist and sn == s5.n) or (s6 and s6.exist and sn == s6.n)
			end

			return false
		end
		
		for j = 1, #line - 2 do
            local c0 = line[j];
            local c1 = line[j + 1]
            local c2 = line[j + 2]
            if(c0 and c0.exist) and (c1 and c1.exist) then
				if (c0.n == c1.n) and checkElement(i, j, false, vertical) then
					return true
				elseif (c2 and c2.exist) and (c0.n == c2.n) and checkElement(j, i, true, vertical) then
					return true
				end
			end
		end
		return false
	end
	
	
    --check vertical moves
	for x = 1, self.w do
		if checkLine(true, x, self.stones[x]) then
			return true
		end
	end
	
	--check horizontal moves
	for y = 1, self.h do
		local row = { }
		for x = 1, self.w do
			row[x] = self.stones[x][y]
		end
	
		if checkLine(false, y, row) then
			return true
		end
	end
	
	return false
end


--=======================================================

function fall(stones)

	function sortLine(stones)
		function swap(tbl, x1, x2)
			local tmp = tbl[x1]
			tbl[x1] = tbl[x2]
			tbl[x2] = tmp
		end

		for x = 1, #stones do
			if not stones[x].exist then
				for y = x, 2, -1 do
					swap(stones, y, y - 1)
				end
			end
		end
	end
	for x = 1, #stones do
		sortLine(stones[x])
		for y = 1, #stones[x] do
			if not stones[x][y].exist  then
				stones[x][y] = RandomStone()
			else
				break
			end
		end
	end
end

function Field:tick()

	for x = 1, self.w do
		for y = 1, self.h do

			local s = self.stones[x][y]
			if s then
				local s1 = self:get(x - 1, y);
				local s2 = self:get(x + 1, y);
				local s3 = self:get(x, y - 1);
				local s4 = self:get(x, y + 1);
				
				if (s1 and s2 and s1.exist and s2.exist) and (s.n == s1.n) and (s.n == s2.n) then
					self.stones[x - 1][y] = Stone:init()
					self.stones[x][y] = Stone:init()
					self.stones[x + 1][y] = Stone:init()
					fall(self.stones)
					return true
				elseif (s3 and s4 and s3.exist and s4.exist) and (s.n == s3.n) and (s.n == s4.n) then
					self.stones[x][y - 1] = Stone:init()
					self.stones[x][y] = Stone:init()
					self.stones[x][y + 1] = Stone:init()
					fall(self.stones)
					return true
				end
			end

		end
	end
	
	return false
end

--=======================================================

function Field:dump()
	os.execute("cls")
	
	local line = "    "
	for x = 0, self.w - 1 do
		line = line .. x .. " "
	end
	print(line, '\n')

	for x = 1, self.h do
		local line = x - 1 .. "   "
		for y = 1, self.w do
			line = line .. self.stones[y][x].n .. " "
		end
		print(line)
	end

	print()

	if self.bm then
		self.bm = false
		print("---- no more moves! field was mixed ----")
	end
	print("---------- make a move please ----------")
	
	-- Decrease the framerate for visual verification
	local sec = tonumber(os.clock() + 3); 
    while (os.clock() < sec) do 
    end 
end

