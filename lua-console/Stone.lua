Stone = { n = ' ', exist = false }

function Stone:init(o, n)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	self.n = ' '
	self.exist = false
	return o
end

--= AStone ==================================

AStone = Stone:init()

function AStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'A'
	self.exist = true
	return o
end

--= BStone ==================================

BStone = Stone:init()

function BStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'B'
	self.exist = true
	return o
end

--= CStone ==================================

CStone = Stone:init()

function CStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'C'
	self.exist = true
	return o
end

--= DStone ==================================

DStone = Stone:init()

function DStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'D'
	self.exist = true
	return o
end

--= EStone ==================================

EStone = Stone:init()

function EStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'E'
	self.exist = true
	return o
end

--= FStone ==================================

FStone = Stone:init()

function FStone:init(o)
	o = o or Stone:init(o)
	setmetatable(o, self)
	self.__index = self
	self.n = 'F'
	self.exist = true
	return o
end

--= StoneRandomizer =========================

__stonesExamples = { }
__stonesExamples[#__stonesExamples+1] = AStone:init()
__stonesExamples[#__stonesExamples+1] = BStone:init()
__stonesExamples[#__stonesExamples+1] = CStone:init()
__stonesExamples[#__stonesExamples+1] = DStone:init()
__stonesExamples[#__stonesExamples+1] = EStone:init()
__stonesExamples[#__stonesExamples+1] = FStone:init()

function RandomStone()
    local sCnt = #(__stonesExamples)
	return __stonesExamples[math.random(1,sCnt)]
end
