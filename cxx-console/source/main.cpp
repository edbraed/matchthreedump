#include <iostream>
#include <string>
#include <vector>
#include <limits>

#include <Stone.h>
#include <Field.h>

#include <memory>

int main(void)
{
    Field field;
    field.init({ 5, 10 });
    field.dump();

    auto errCmd = []()
    {
        std::cout << "error command! Expected commands 'exit', 'm <i> <j> <l/r/u/d>'" << std::endl;
    };

    while(true)
    {
        std::string input;
        std::cin >> input;

        if(input == "exit")
            break;
        else if(input == "m")
        {
            // Обрабатываем ввод
            unsigned i, j;
            char t;
            std::cin >> i >> j >> t;
            StepType s = CharToStepType(t);
            if(i > field.width() || j > field.height() || s == StepType::err)
            {
                errCmd();
                continue;
            }

            // Делаем ход
            field.move({ i, j }, s);

            // Тикаем до тех пор пока на поле остаются изменения
            do
            {
                field.dump();
            }
            while(field.tick());

            // В случае отсутствия ходов, мешаем поле
            while(!field.movesAvaliable())
            {
                field.mix();
                field.dump();
            }
        }
        else
        {
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            errCmd();
        }
    }


    std::cout << "end" << std::endl;
    return 0;
}

