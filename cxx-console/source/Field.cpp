#include <Field.h>

#include <thread>
#include <chrono>
#include <algorithm>

StepType CharToStepType(char t)
{
    if     (t == 'l') return StepType::l;
    else if(t == 'r') return StepType::r;
    else if(t == 'u') return StepType::u;
    else if(t == 'd') return StepType::d;
    return StepType::err;
}

size_t Field::width()
{
    return stones.size();
}

size_t Field::height()
{
    return stones.size() > 0 ? stones[0].size() : 0;
}

StonePtr& getStone(std::vector<std::vector<StonePtr>>& stones, size_t x, size_t y)
{
    static StonePtr nulStone;
    if(x >= stones.size() || y >= stones[0].size())
        return nulStone;
    return stones[x][y];
};

void Field::init(uvec2 sz, size_t seed/*= 0*/)
{
    std::srand(seed);
    stones = std::vector<std::vector<std::unique_ptr<Stone>>>(sz.x);
    for(auto& row : stones)
    {
        row = std::vector<std::unique_ptr<Stone>>(sz.y);
        for(auto& stone : row)
            stone = rnd.RandomStone();
    }

    // Сразу протикаем все возможные совпадения
    while(tick());
}

void Field::move(uvec2 pt, StepType s)
{
    if(s == StepType::l && pt.x > 0)
        std::swap(stones[pt.x][pt.y], stones[pt.x - 1][pt.y]);
    else if(s == StepType::r && pt.x < (width() - 1))
        std::swap(stones[pt.x][pt.y], stones[pt.x + 1][pt.y]);
    else if(s == StepType::u && pt.y > 0)
        std::swap(stones[pt.x][pt.y], stones[pt.x][pt.y - 1]);
    else if(s == StepType::d && pt.y < (height() - 1))
        std::swap(stones[pt.x][pt.y], stones[pt.x][pt.y + 1]);
}

void Field::mix()
{
    for(size_t x = 0; x < stones.size(); x++)
    {
        for(size_t y = 0; y < stones[x].size(); y++)
        {
            unsigned r = (std::rand() % 4) + 1;
            StepType s = static_cast<StepType>(r);
            move({ x, y }, s);
        }
    }
    m_bMixed = true;
}

bool Field::movesAvaliable()
{
    auto checkLine = [&](bool vertical, size_t i, std::vector<StonePtr>& ln)
    {
        // Создаём список пар в тройках
        // Пара может идти подряд "XXY или через 1 "XYX"
        struct spair { uvec2 p{0,0}; bool t{false}; };
        std::vector<spair> pairs;
        pairs.reserve(ln.size() / 2);
        for(size_t j = 0; j < ln.size() - 2; j++)
        {
            auto& c0 = ln[j];
            auto& c1 = ln[j + 1];
            auto& c2 = ln[j + 2];
            if(!c0 || !c1)
                continue;

            uvec2 p = vertical ? uvec2{ i, j } : uvec2{ j, i };

            if(c0->n == c1->n)
                pairs.push_back({ p, false });
            else if(c2 && c0->n == c2->n)
                pairs.push_back({ p, true });
        }

        // Check
        // При t=true   [ ][?][ ]   При t=false   [ ][?][ ][ ][?][ ]
        //              [x][ ][x]                 [?][ ][x][x][ ][?]
        //              [ ][?][ ]                 [ ][?][ ][ ][?][ ]
        for(const auto& pair : pairs)
        {
            auto pp = pair.p;
            auto sn = getStone(stones, pp.x, pp.y)->n;
            if(pair.t)
            {
                auto& s1 = vertical ? getStone(stones, pp.x - 1, pp.y + 1) :
                                      getStone(stones, pp.x + 1, pp.y - 1);
                auto& s2 = vertical ? getStone(stones, pp.x + 1, pp.y + 1) :
                                      getStone(stones, pp.x + 1, pp.y + 1);
                if((s1 && sn == s1->n) || (s2 && sn == s2->n))
                    return true;
            }
            else
            {
                auto& s1 = vertical ? getStone(stones, pp.x, pp.y - 2) :
                                      getStone(stones, pp.x - 2, pp.y);
                auto& s2 = vertical ? getStone(stones, pp.x, pp.y + 3) :
                                      getStone(stones, pp.x + 3, pp.y);
                auto& s3 = vertical ? getStone(stones, pp.x - 1, pp.y - 1) :
                                      getStone(stones, pp.x - 1, pp.y - 1);
                auto& s4 = vertical ? getStone(stones, pp.x + 1, pp.y - 1) :
                                      getStone(stones, pp.x - 1, pp.y + 1);
                auto& s5 = vertical ? getStone(stones, pp.x - 1, pp.y + 2) :
                                      getStone(stones, pp.x + 2, pp.y - 1);
                auto& s6 = vertical ? getStone(stones, pp.x + 1, pp.y + 2) :
                                      getStone(stones, pp.x + 2, pp.y + 1);

                if((s1 && sn == s1->n) || (s2 && sn == s2->n) ||
                   (s3 && sn == s3->n) || (s4 && sn == s4->n) ||
                   (s5 && sn == s5->n) || (s6 && sn == s6->n))
                    return true;
            }
        }

        return false;
    };

    // check vertical moves
    for(size_t x = 0; x < stones.size(); x++)
    {
        if(checkLine(true, x, stones[x]))
            return true;
    }

    // check horizontal moves
    for(size_t y = 0; y < stones[0].size(); y++)
    {
        std::vector<StonePtr> row(stones.size());
        for(size_t x = 0; x < stones.size(); x++)
            row[x] = stones[x][y] ? std::make_unique<Stone>(*stones[x][y]) : nullptr;

        if(checkLine(false, y, row))
            return true;
    }
    return false;
}

bool Field::tick()
{
    auto fall = [&]()
    {
        for(size_t x = 0; x < stones.size(); x++)
        {
            std::sort(stones[x].begin(), stones[x].end(),
                      [](auto& a, auto& b) { return !a && b; });
            for(size_t y = 0; y < stones[x].size(); y++)
            {
                if(!stones[x][y])
                    stones[x][y] = rnd.RandomStone();
                else
                    break;
            }
        }
    };

    for(size_t x = 0; x < stones.size(); x++)
    {
        for(size_t y = 0; y < stones[x].size(); y++)
        {
            auto& s = stones[x][y];
            if(!s)
                continue;

            auto& s1 = getStone(stones, x - 1, y);
            auto& s2 = getStone(stones, x + 1, y);
            auto& s3 = getStone(stones, x, y - 1);
            auto& s4 = getStone(stones, x, y + 1);

            if((s1 && s2) && (s->n == s1->n) && (s->n == s2->n))
            {
                s.release();
                s1.release();
                s2.release();
                fall();
                return true;
            }
            else if((s3 && s4) && (s->n == s3->n) && (s->n == s4->n))
            {
                s.release();
                s3.release();
                s4.release();
                fall();
                return true;
            }
        }
    }

    return false;
}

void Field::dump()
{
    system("cls");
    std::cout << "    ";
    for(size_t i = 0; i < width(); i++)
        std::cout << i << " ";
    std::cout << std::endl << std::endl;

    for(size_t i = 0; i < height(); i++)
    {
        std::cout << i << "   ";
        for(size_t j = 0; j < width(); j++)
        {
            if(stones[j][i])
                std::cout << stones[j][i]->n << " ";
            else
                std::cout << ' ' << " ";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;

    if(m_bMixed)
    {
        m_bMixed = false;
        std::cout << "---- no more moves! field was mixed ----" << std::endl;
    }
    std::cout << "---------- make a move please ----------" << std::endl;

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1000ms);
}
