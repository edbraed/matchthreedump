## Match three console game 
  
The simplest implementation of the classic "match three" in the console on cxx.  
  
Supports only 2 commands:  
* **exit** - terminate app  
* **m i j l|r|u|d** - make a move  
  *  where 'i' and 'j' are field coordinates,  
  *  and 'l' or 'r' or 'u' or 'd' (left, right, up, down move) 

