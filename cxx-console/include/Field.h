#pragma once

#include <iostream>
#include <memory>
#include <vector>

#include <Stone.h>

enum StepType
{
    err = 0,
    l   = 1,
    r   = 2,
    u   = 3,
    d   = 4
};

StepType CharToStepType(char t);

/////////////////////////////////////////////////////////////////////////////
///=- Field -=///////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//class Field;
//typedef void (FieldStepper)(Field*);

struct uvec2
{
    size_t x, y;
};

class Field final
{
    private:
        bool m_bMixed {false};

    public:
        std::vector<std::vector<StonePtr>> stones;
        StoneRandomizer rnd;

        size_t width();
        size_t height();

        void init(uvec2 sz, size_t seed = 0);
        void move(uvec2 pt, StepType s);
        void mix();
        bool movesAvaliable();
        bool tick();
        void dump();
};
