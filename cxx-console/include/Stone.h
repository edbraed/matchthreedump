#pragma once

#include <memory>

class Stone
{
    public:
        char n;

        Stone(char n);

        //virtual bool compare();
        //virtual void destroy();
};

typedef std::unique_ptr<Stone> StonePtr;

class AStone : public Stone
{
    public:
        AStone();
};

class BStone : public Stone
{
    public:
        BStone();
};

class CStone : public Stone
{
    public:
        CStone();
};

class DStone : public Stone
{
    public:
        DStone();
};

class EStone : public Stone
{
    public:
        EStone();
};

class FStone : public Stone
{
    public:
        FStone();
};

class StoneRandomizer
{
    public:
        std::unique_ptr<Stone> RandomStone();
};
