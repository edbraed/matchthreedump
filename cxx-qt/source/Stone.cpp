#include <Stone.h>

#include <random>
#include <typeinfo.h>

#include <QDebug>

/////////////////////////////////////////////////////////////////////////////
///=- Abstract Stone -=//////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

IMPL_STONE(Stone);
size_t Stone::Score() const { return 0; }
std::string Stone::GetRes() const { return ""; }
size_t Stone::GetTID() const { return typeid(*this).hash_code(); }

/////////////////////////////////////////////////////////////////////////////
///=- Stones implementation -=///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

IMPL_STONE(Ruby);
size_t Ruby::Score()          const { return 1; }
std::string Ruby::GetRes()    const { return ":/stones/ruby.png"; }

IMPL_STONE(Sapphire);
size_t Sapphire::Score()       const { return 1; }
std::string Sapphire::GetRes() const { return ":/stones/sapphire.png"; }

IMPL_STONE(Emerald);
size_t Emerald::Score()        const { return 1; }
std::string Emerald::GetRes()  const { return ":/stones/emerald.png"; }

IMPL_STONE(Topaz);
size_t Topaz::Score()          const { return 1; }
std::string Topaz::GetRes()    const { return ":/stones/topaz.png"; }

IMPL_STONE(Amethyst);
size_t Amethyst::Score()       const { return 1; }
std::string Amethyst::GetRes() const { return ":/stones/amethyst.png"; }

IMPL_STONE(Alexandrite);
size_t Alexandrite::Score()       const { return 1; }
std::string Alexandrite::GetRes() const { return ":/stones/alexandrite.png"; }

/////////////////////////////////////////////////////////////////////////////
///=- Stones randomizer -=///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

std::unique_ptr<Stone> StoneRandomizer::RandomStone()
{
    auto stonesCnt = StoneImplementer::Creators().size();
    unsigned rndNum = (std::rand() % (stonesCnt - 1)) + 1;
    //qDebug() << "GetRes in randomizer : " << ptr->GetRes();
    return StoneImplementer::Creators()[rndNum]();
}
