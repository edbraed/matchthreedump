#include <FieldModel.h>

StepType CharToStepType(char t)
{
    if     (t == 'l') return StepType::l;
    else if(t == 'r') return StepType::r;
    else if(t == 'u') return StepType::u;
    else if(t == 'd') return StepType::d;
    return StepType::err;
}

int FieldModel::width()
{
    return m_stones.size();
}

int FieldModel::height()
{
    return m_stones.size() > 0 ? m_stones[0].size() : 0;
}

const StoneMatrix& FieldModel::stones() const
{
    return m_stones;
}

size_t FieldModel::score() const
{
    return m_score;
}

StonePtr& FieldModel::getStone(ivec2 pt)
{
    static StonePtr nulStone;
    if(pt.x >= m_stones.size() || pt.y >= m_stones[0].size())
        return nulStone;
    return m_stones[pt.x][pt.y];
};

void FieldModel::init(ivec2 sz, size_t seed/*= 0*/)
{
    sz.y++;
    std::srand(seed);
    m_stones = std::vector<std::vector<std::unique_ptr<Stone>>>(sz.x);
    for(auto& row : m_stones)
    {
        row = std::vector<std::unique_ptr<Stone>>(sz.y);
        for(size_t x = 1; x < row.size(); x++)
            row[x] = rnd.RandomStone();
    }

    // Сразу протикаем все возможные совпадения
    while(tick());
}

bool FieldModel::move(ivec2 pt, StepType s)
{
    ivec2 a = pt;
    ivec2 b = { -1, -1 };

    if(s == StepType::l && pt.x > 0)
        b = {pt.x - 1, pt.y};
    else if(s == StepType::r && pt.x < (width() - 1))
        b = {pt.x + 1, pt.y};
    else if(s == StepType::u && pt.y > 0)
        b = {pt.x, pt.y - 1};
    else if(s == StepType::d && pt.y < (height() - 1))
        b = {pt.x, pt.y + 1};

    if(b.x < 0 || b.y < 0)
        return false;

    auto& sa = getStone(a);
    auto& sb = getStone(b);
    if(!sa || !sb)
        return false;
    std::swap(sa, sb);

    auto checkLine = [](auto& ln)
    {
        for(size_t x = 1; x < ln.size() - 1; x++)
        {
            auto& s0 = ln[x - 1];
            auto& s1 = ln[x];
            auto& s2 = ln[x + 1];

            if( !*(s0) || !*(s1) || !*(s2))
                continue;
            auto m = (*s1)->GetTID();
            if((*s0)->GetTID() == m && (*s2)->GetTID() == m)
                return true;
        }
        return false;
    };

    auto checkMoves = [&](ivec2 pt)
    {
        auto& s  = getStone(pt);
        auto& s0 = getStone({pt.x - 2, pt.y});
        auto& s1 = getStone({pt.x - 1, pt.y});
        auto& s2 = getStone({pt.x + 1, pt.y});
        auto& s3 = getStone({pt.x + 2, pt.y});
        auto& s4 = getStone({pt.x, pt.y - 2});
        auto& s5 = getStone({pt.x, pt.y - 1});
        auto& s6 = getStone({pt.x, pt.y + 1});
        auto& s7 = getStone({pt.x, pt.y + 2});

        std::vector<StonePtr*> ln1 = {&s0, &s1, &s, &s2, &s3 };
        std::vector<StonePtr*> ln2 = {&s4, &s5, &s, &s6, &s7 };
        if(checkLine(ln1) || checkLine(ln2))
            return true;

        return false;
    };

    if(checkMoves(a) || checkMoves(b))
        return true;
    std::swap(sa, sb);
    return false;
}

void FieldModel::mix()
{
    ivec2 sz = { static_cast<int>(m_stones.size()),
                 static_cast<int>(m_stones[0].size()) - 1};
    for(int x = 0; x < m_stones.size(); x++)
    {
        for(int y = 1; y < m_stones[x].size(); y++)
        {
            ivec2 rnd = { std::rand() % sz.x,
                          (std::rand() % sz.y) + 1 };
            std::swap(m_stones[x][y], m_stones[rnd.x][rnd.y]);
        }
    }
}

bool FieldModel::movesAvaliable()
{
    auto checkLine = [&](bool vertical, int i, std::vector<TID>& ln)
    {
        // Создаём список пар в тройках
        // Пара может идти подряд "XXY или через 1 "XYX"
        struct spair { ivec2 p{0,0}; bool t{false}; };
        std::vector<spair> pairs;
        pairs.reserve(ln.size() / 2);
        for(int j = 0; j < ln.size() - 2; j++)
        {
            auto& c0 = ln[j];
            auto& c1 = ln[j + 1];
            auto& c2 = ln[j + 2];
            if(c0 == nullTID || c1 == nullTID)
                continue;

            ivec2 p = vertical ? ivec2{ i, j } : ivec2{ j, i };

            if(c0 == c1)
                pairs.push_back({ p, false });
            else if(c2 != nullTID && c0 == c2)
                pairs.push_back({ p, true });
        }

        // Check
        // При t=true   [ ][?][ ]   При t=false   [ ][?][ ][ ][?][ ]
        //              [x][ ][x]                 [?][ ][x][x][ ][?]
        //              [ ][?][ ]                 [ ][?][ ][ ][?][ ]
        for(const auto& pair : pairs)
        {
            auto pp = pair.p;
            auto sn = getStone(pp)->GetTID();
            if(pair.t)
            {
                auto& s1 = vertical ? getStone({ pp.x - 1, pp.y + 1 }) :
                                      getStone({ pp.x + 1, pp.y - 1 });
                auto& s2 = vertical ? getStone({ pp.x + 1, pp.y + 1 }) :
                                      getStone({ pp.x + 1, pp.y + 1 });
                if((s1 && sn == s1->GetTID()) || (s2 && sn == s2->GetTID()))
                    return true;
            }
            else
            {
                auto& s1 = vertical ? getStone({ pp.x, pp.y - 2 }) :
                                      getStone({ pp.x - 2, pp.y });
                auto& s2 = vertical ? getStone({ pp.x, pp.y + 3 }) :
                                      getStone({ pp.x + 3, pp.y });
                auto& s3 = vertical ? getStone({ pp.x - 1, pp.y - 1 }) :
                                      getStone({ pp.x - 1, pp.y - 1 });
                auto& s4 = vertical ? getStone({ pp.x + 1, pp.y - 1 }) :
                                      getStone({ pp.x - 1, pp.y + 1 });
                auto& s5 = vertical ? getStone({ pp.x - 1, pp.y + 2 }) :
                                      getStone({ pp.x + 2, pp.y - 1 });
                auto& s6 = vertical ? getStone({ pp.x + 1, pp.y + 2 }) :
                                      getStone({ pp.x + 2, pp.y + 1 });

                if((s1 && sn == s1->GetTID()) || (s2 && sn == s2->GetTID()) ||
                   (s3 && sn == s3->GetTID()) || (s4 && sn == s4->GetTID()) ||
                   (s5 && sn == s5->GetTID()) || (s6 && sn == s6->GetTID()))
                    return true;
            }
        }

        return false;
    };

    // check vertical moves
    for(size_t x = 0; x < m_stones.size(); x++)
    {
        std::vector<TID> coll(m_stones[x].size());
        for(size_t y = 0; y < coll.size(); y++)
            coll[y] = m_stones[x][y] ? m_stones[x][y]->GetTID() : nullTID;

        if(checkLine(true, x, coll))
            return true;
    }

    // check horizontal moves
    for(size_t y = 0; y < m_stones[0].size(); y++)
    {
        std::vector<TID> row(m_stones.size());
        for(size_t x = 0; x < m_stones.size(); x++)
            row[x] = m_stones[x][y] ? m_stones[x][y]->GetTID() : nullTID;

        if(checkLine(false, y, row))
            return true;
    }
    return false;
}


