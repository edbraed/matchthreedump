#include "FieldView.h"
#include "ui_FieldView.h"

#include <set>

#include <QMouseEvent>
#include <QPainter>
#include <QDebug>
#include <QTimer>
#include <QDebug>

#include <Stone.h>

constexpr size_t renderRate = 16; // render rate (in ms)
constexpr size_t updateRate = 2; // update rate (for each 'n' frame)
constexpr size_t particlesFrames = 15; // frames count for particles animation
constexpr float lerpCoof = 0.2; // stone move, lerp coefficient
constexpr float ctrlSens = 0.4; // move access sensetive (in percent)
constexpr int fvSizeCorrect = 100; // correct size and offset field (in pixels)


FieldView::FieldView(QWidget *parent) : QWidget(parent),
                                        ui(new Ui::FieldView)

{
    ui->setupUi(this);
    ui->lblScore->setAttribute(Qt::WA_TranslucentBackground);

    m_field.init({1, 1}, 0);

    m_loop = new QTimer(this);
    connect(m_loop, SIGNAL(timeout()), this, SLOT(mainLoop()));
    m_loop->start(renderRate);
}

FieldView::~FieldView()
{
    delete ui;
}

//===========================================================================
//=- Common behavior -=======================================================
//===========================================================================

void FieldView::SetCount(const QSize& sz)
{
    m_field.init({sz.width(), sz.height()}, 0);
    ResetFieldView();
}

void FieldView::ResetFieldView()
{
    const int fw = m_field.width();
    const int fh = m_field.height();
    m_fvCellSz = (std::min(width(), height()) - fvSizeCorrect) / std::max(fw, fh);
    m_fvBeg = { (width() -  m_fvCellSz * fw ) / 2,
                (height() - m_fvCellSz * fh) / 2 };
    m_spriteMgr.Init({fw, fh}, size(), m_fvCellSz, m_fvBeg);
    m_particleMgr.Init(m_spriteMgr.GetAllStones(),
                       particlesFrames, m_fvCellSz, m_fvBeg);
}

void FieldView::resizeEvent(QResizeEvent *event)
{
    ResetFieldView();
}

//===========================================================================
//=- Rendering and updating -================================================
//===========================================================================

void FieldView::mainLoop()
{
    if(m_currFrame++ == updateRate)
    {
        m_currFrame = 0;
        // logic update
        auto tickCallBack = [&](StonePtr& s, ivec2 p)
        {
            m_particleMgr.Add({p.x, p.y}, s->GetTID());
        };
        m_ticksEnd = !m_field.tick(tickCallBack);

        if(m_ticksEnd && !m_field.movesAvaliable())
            m_field.mix();

        auto newScore = m_field.score();
        if(newScore != m_oldScore)
        {
            ui->lblScore->setText("<html><head/><body><p><span style=\" color:#ffffff;\">Score: " +
                                  QString::number(newScore) +  "</span></p></body></html>");
            m_oldScore = newScore;
        }
    }

    // render
    this->update();
}

QPointF Lerp(QPointF a, QPointF b, float t)
{
    return { a.x() + t * (b.x() - a.x()),
             a.y() + t * (b.y() - a.y()) };
}

void FieldView::paintEvent(QPaintEvent*)
{
    QPainter p;
    p.begin(this);
    p.setRenderHint(QPainter::Antialiasing, true);

    const QImage& background = m_spriteMgr.GetBackground();
    const QPoint cntr = { width() / 2 - background.width() / 2,
                          height() / 2 - background.height() / 2 };
    p.drawImage(cntr, background);

    p.drawImage(m_fvBeg, m_spriteMgr.GetCellBack());

    // update visual model
    std::set<Stone*> eStones; // existing stones
    for(int x = 0; x < m_field.width(); x++)
    {
        for(int y = 0; y < m_field.height(); y++)
        {
            const auto pStone = m_field.stones()[x][y].get();
            if(!pStone)
                continue;
            const auto& it = m_vizFrame.find(pStone);
            QPointF nCoord = QPointF{float(x), float(y)};
            if(it != m_vizFrame.end())
            {
                //QPointF l = Lerp(it->second, nCoord, lerpCoof);
                //it->second = { (nCoord.x() - l.x()) < 0.1 ? nCoord.x() : l.x(),
                //               (nCoord.y() - l.y()) < 0.1 ? nCoord.y() : l.y() };
                it->second = Lerp(it->second, nCoord, lerpCoof);
            }
            else
                m_vizFrame.insert({pStone, nCoord});

            eStones.insert(pStone);
        }
    }

    // render visual model
    for(auto it = m_vizFrame.begin(); it != m_vizFrame.end(); it++)
    {
        const auto& sIt = eStones.find((*it).first);

        QPoint off = m_fvBeg;
        QPointF pos = (*it).second;
        QPoint rXY = QPoint{qRound((*it).second.x()),
                            qRound((*it).second.y()) };

        //if(rXY.y() == 0) // hide first row!!
        //    continue;

        if(rXY == m_pressCell)
        {
            off = { off.x() + m_pressOffset.x(),
                    off.y() + m_pressOffset.y() };
        }
        else if(rXY == m_pressOppos)
        {
            off = { off.x() - m_pressOffset.x(),
                    off.y() - m_pressOffset.y() };
        }
        const auto& sprite = m_spriteMgr.GetStone((*it).first->GetTID());
        QPoint fp = {static_cast<int>(m_fvCellSz * pos.x()),
                     static_cast<int>(m_fvCellSz * pos.y())};
        p.drawImage(QPoint{fp.x() + off.x(),
                           fp.y() + off.y()}, sprite);
    }

    // remove not exist stones from visual model
    for(auto it = m_vizFrame.begin(); it != m_vizFrame.end(); it++)
    {
        if(eStones.find((*it).first) == eStones.end())
            it = m_vizFrame.erase(it);
    }

    if(m_pressOppos.x() > -1)
    {
        p.drawImage(QPoint{m_fvCellSz * m_pressOppos.x() + m_fvBeg.x(),
                           m_fvCellSz * m_pressOppos.y() + m_fvBeg.y()},
                    m_spriteMgr.GetCellSelect());
    }

    // draw spawns
    p.drawImage(m_fvBeg, m_spriteMgr.GetCellSpawn());

    // draw particles
    m_particleMgr.Draw(p);

    p.end();
}

//===========================================================================
//=- Input process -=========================================================
//===========================================================================

int border(int v, int b)
{
    return v < -b ? -b : (v > b ? b : v);
}

void FieldView::mousePressEvent(QMouseEvent* event)
{
    if(m_ticksEnd && event->button() == Qt::LeftButton)
    {
        const auto& mPos = event->pos();
        QPoint maxpt = {width() - m_fvBeg.x(), height() - m_fvBeg.y() };
        if(mPos.x() < m_fvBeg.x() || mPos.y() < m_fvBeg.y() ||
           mPos.x() > maxpt.x()   || mPos.y() > maxpt.y())
            return;
        m_pressPoint = mPos;

        auto locPos = mPos - m_fvBeg;
        QPoint fsz = { m_field.width(), m_field.height() };
        QPointF normPos = { locPos.x() / static_cast<float>(m_fvCellSz * fsz.x()),
                            locPos.y() / static_cast<float>(m_fvCellSz * fsz.y())};;

        m_pressCell = { static_cast<int>(normPos.x() * fsz.x()),
                        static_cast<int>(normPos.y() * fsz.y()) };

        m_loop->stop();
    }
}

void FieldView::mouseMoveEvent(QMouseEvent *event)
{
    if(m_pressCell.x() > -1)
    {
        QPoint deltaPos = event->pos() - m_pressPoint;
        QPoint absDeltaPos = { std::abs(deltaPos.x()),
                               std::abs(deltaPos.y()) };

        m_pressOffset = { 0, 0 };

        const int hCellSz = m_fvCellSz * ctrlSens;
        if(absDeltaPos.x() > absDeltaPos.y())
        {
            m_pressOffset.setX(border(deltaPos.x(), m_fvCellSz));
            m_pressOppos = { -1, -1 };
            if(absDeltaPos.x() > hCellSz)
            {
                m_pressOppos = { deltaPos.x() > 0 ? m_pressCell.x() + 1 :
                                                    m_pressCell.x() - 1,
                                 m_pressCell.y()};
            }
        }
        else
        {
            m_pressOffset.setY(border(deltaPos.y(), m_fvCellSz));
            m_pressOppos = { -1, -1 };
            if(absDeltaPos.y() > hCellSz)
            {
                m_pressOppos = { m_pressCell.x(),
                                 deltaPos.y() > 0 ? m_pressCell.y() + 1 :
                                                    m_pressCell.y() - 1,
                                 };
            }
        }
        this->update();
    }
}

void FieldView::mouseReleaseEvent(QMouseEvent* event)
{
    auto resetPressVars = [&]()
    {
        m_pressCell   = { -1, -1 };
        m_pressPoint  = { -1, -1 };
        m_pressOffset = { -1, -1 };
        m_pressOppos  = { -1, -1 };
        this->update();
    };

    if(event->button() == Qt::LeftButton)
    {
        if(m_pressOppos.x() < 0 || m_pressCell.x() < 0)
        {
            resetPressVars();
            return;
        }

        StepType st = err;
        if(m_pressOppos.x() < m_pressCell.x())
            st = StepType::l;
        else if(m_pressOppos.x() > m_pressCell.x())
            st = StepType::r;
        else if(m_pressOppos.y() < m_pressCell.y())
            st = StepType::u;
        else if(m_pressOppos.y() > m_pressCell.y())
            st = StepType::d;

        if(m_field.move({m_pressCell.x(), m_pressCell.y()}, st))
        {
            auto& sa = m_field.stones()[m_pressOppos.x()][m_pressOppos.y()];
            auto& sb = m_field.stones()[m_pressCell.x()][m_pressCell.y()];
            std::swap(m_vizFrame[sa.get()], m_vizFrame[sb.get()]);
        }

        resetPressVars();
        m_loop->start(renderRate);
    }
}
