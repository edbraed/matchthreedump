#include <SpriteManager.h>

#include <QPainter>

void SpriteManager::Init(QSize fsz, QSize vsz, int spriteSize, QPoint fvBeg)
{
    auto resMap = StoneImplementer::ResourceMap();

    m_stoneSprites.clear();
    for(auto& res : resMap)
    {
        if(res.second.size() > 0)
        {
            auto sprite = QImage(res.second.c_str());
            sprite = sprite.scaled(QSize(spriteSize, spriteSize));
            m_stoneSprites.insert({res.first, sprite});
        }
        else
            m_stoneSprites.insert({res.first, QImage()});
    }

    // background sprite
    auto back = QImage(":/field/background.png");
    m_fieldSprites[0] = back.scaled(vsz, Qt::AspectRatioMode::KeepAspectRatioByExpanding);


    // background cell sprite
    fvBeg *= 2;
    m_fieldSprites[1] = QImage(vsz.width() - fvBeg.x(),
                               vsz.height() - fvBeg.y(), QImage::Format_RGBA8888);
    m_fieldSprites[1].fill(QColor{255, 255, 255, 0});
    QPainter p;
    p.begin(&m_fieldSprites[1]);
    QImage cell = QImage(":/field/cell_back.png").scaled(spriteSize, spriteSize);
    for(int x = 0; x < fsz.width(); x++)
    {
        for(int y = 0; y < fsz.height(); y++)
            p.drawImage(QPoint{spriteSize * x, spriteSize * y}, cell);
    }
    p.end();

    // select sprite
    m_fieldSprites[2] = QImage(":/field/cell_select.png").scaled(spriteSize, spriteSize);

    // spawn sprite
    m_fieldSprites[3] = QImage(vsz.width() - fvBeg.x(), spriteSize, QImage::Format_RGBA8888);
    m_fieldSprites[3].fill(QColor{255, 255, 255, 0});
    p.begin(&m_fieldSprites[3]);
    QImage spawnImg = QImage(":/field/cell_spawn.png").scaled(spriteSize, spriteSize);
    for(int x = 0; x < fsz.width(); x++)
        p.drawImage(QPoint{spriteSize * x, 0}, spawnImg);
}

const std::map<TID, QImage>& SpriteManager::GetAllStones() const
{
    return m_stoneSprites;
}

const QImage& SpriteManager::GetStone(TID sTID) const
{
    return m_stoneSprites.find(sTID)->second;
}

const QImage& SpriteManager::GetBackground() const
{
    return m_fieldSprites[0];
}

const QImage& SpriteManager::GetCellBack() const
{
    return m_fieldSprites[1];
}

const QImage& SpriteManager::GetCellSelect() const
{
    return m_fieldSprites[2];
}

const QImage& SpriteManager::GetCellSpawn() const
{
    return m_fieldSprites[3];
}

