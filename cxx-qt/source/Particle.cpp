#include <Particle.h>

#include <QDebug>

ParticleManager::ParticleManager() { }

void ParticleManager::Init(const std::map<TID, QImage>& sprites,
                           size_t fCount, int cellSz, QPoint fvBeg)
{
    m_framesCount = fCount;
    m_cellSz = cellSz;
    m_imgs.clear();
    m_fvBeg = fvBeg;

    QPainter p;
    for(auto& sp : sprites)
    {
        const QImage& image = sp.second;
        if(image.isNull())
            continue;
        std::array<QImage, 4> parts;

        QSize pSz = { image.width() / 2, image.height() / 2 };
        parts.fill({pSz, QImage::Format_RGBA8888});
        auto drwPart = [&](QPoint pt, QImage& res)
        {
            res.fill(QColor(0, 0, 0, 0));
            p.begin(&res);
            p.drawImage(pt, image);
            p.end();
        };
        drwPart({    0,            0        }, parts[0]);
        drwPart({-pSz.width(),     0        }, parts[1]);
        drwPart({    0,        -pSz.height()}, parts[2]);
        drwPart({-pSz.width(), -pSz.height()}, parts[3]);

        m_imgs.insert({sp.first, parts});
    }
}

QImage ParticleManager::GetFrame(Particle& part)
{
    QSize pSz = m_imgs[part.tid][0].size();

    const double l = static_cast<double>(part.currFrame) / m_framesCount;
    QImage result(pSz * 4, QImage::Format_RGBA8888);
    result.fill(QColor(0, 0, 0, 0));

    auto gc = [&pSz, l, &result](int xm, int ym)
    {
        QPoint pSzP = {pSz.width(), pSz.height() };
        QPoint cntr = { result.width() / 2, result.height() / 2 };
        QPoint m = { int(l * xm * pSz.width()),
                     int(l * ym *  pSz.height()) };
        QPoint mnm = {std::max(-xm, 0), std::max(-ym, 0) };
        return QPoint{ (cntr.x() - pSzP.x() * mnm.x()) + m.x(),
                       (cntr.y() - pSzP.y() * mnm.y()) + m.y() };
    };

    QPainter p;
    p.begin(&result);
    p.setOpacity(1.0 - l);
    p.drawImage(gc(-1, -1), m_imgs[part.tid][0]);
    p.drawImage(gc( 1, -1), m_imgs[part.tid][1]);
    p.drawImage(gc(-1,  1), m_imgs[part.tid][2]);
    p.drawImage(gc( 1,  1), m_imgs[part.tid][3]);
    p.end();

    part.currFrame++;
    return result;
}

void ParticleManager::Add(QPoint p, size_t tid)
{
    if(m_framesCount > 0)
        m_particles.emplace_back(Particle{p, tid, 0});
}

void ParticleManager::Draw(QPainter& painter)
{
    if(m_framesCount > 0 && m_particles.size() > 0)
    {
        for(auto it = m_particles.begin(); it != m_particles.end(); it++)
        {
            QImage pImg = GetFrame(*it);
            QPoint pp = it->pt;
            int hfvCellSz = m_cellSz / 2;
            pp = { pp.x() * m_cellSz - hfvCellSz + m_fvBeg.x(),
                   pp.y() * m_cellSz - hfvCellSz + m_fvBeg.y() };
            painter.drawImage(pp, pImg);
        }

        for(auto it = m_particles.begin(); it != m_particles.end(); it++)
        {
            if((*it).currFrame == m_framesCount)
                it = m_particles.erase(it);
        }
    }
}
