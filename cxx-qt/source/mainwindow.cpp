#include <mainwindow.h>
#include "ui_mainwindow.h"

#include <FieldView.h>

#include <QDebug>

// Ещё одно отличие
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->fieldView->SetCount({7, 6});
}

MainWindow::~MainWindow()
{
    delete ui;
}
