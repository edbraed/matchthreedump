## Match three QT 
  
Simple Implementation "match three" game on QtWidgets.  
QPainter is used as the basis for rendering.  
  
<img src="images/img.gif" width = "635" height = "504"/>  
  
Dependencies: C++14, Qt  

Game started as a test-job and then continued as a qt pet project.