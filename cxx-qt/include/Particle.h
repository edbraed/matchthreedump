#pragma once

#include <memory>
#include <array>
#include <QImage>
#include <QPainter>

#include <Stone.h>

struct Particle
{
    QPoint pt        { 0, 0 };
    size_t tid       { 0 };
    size_t currFrame { 0 };
};

class ParticleManager
{
    private:
        size_t m_framesCount { 0 };
        int m_cellSz { 0 };
        QPoint m_fvBeg { 0, 0 };
        std::map<size_t, std::array<QImage, 4>> m_imgs { };
        std::list<Particle> m_particles { };

        QImage GetFrame(Particle& part);

    public:

        ParticleManager();

        void Init(const std::map<TID, QImage>& sprites,
                  size_t fCount, int cellSz, QPoint fvBeg);
        void Add(QPoint p, size_t tid);
        void Draw(QPainter& painter);
};
