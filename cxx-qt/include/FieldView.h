#pragma once

#include <QWidget>
#include <QSize>

#include <FieldModel.h>
#include <Particle.h>
#include <SpriteManager.h>

namespace Ui {
class FieldView;
}

class FieldView : public QWidget
{
    Q_OBJECT

public slots:
    void mainLoop();

public:
    explicit FieldView(QWidget *parent = nullptr);
    ~FieldView();

    void SetCount(const QSize& sz);

private:
    Ui::FieldView *ui;
    FieldModel m_field    { };
    QPoint     m_fvBeg    { 0, 0 }; // field view begin offset (coord)
    int        m_fvCellSz { 0 };    // field view cell size
    size_t     m_oldScore { 0 };
    bool       m_ticksEnd { true };
    size_t     m_currFrame { 0 };
    SpriteManager   m_spriteMgr;
    ParticleManager m_particleMgr;

    QPoint m_pressCell   { -1, -1 };
    QPoint m_pressOppos  { -1, -1 };
    QPoint m_pressPoint  { -1, -1 };
    QPoint m_pressOffset { -1, -1 };

    QTimer* m_loop { nullptr };

    std::map<Stone*, QPointF> m_vizFrame;

    void ResetFieldView();
    void resizeEvent(QResizeEvent *event) override;

    void paintEvent(QPaintEvent*) override;

    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
};
