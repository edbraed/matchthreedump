#pragma once

#include <Stone.h>

#include <map>
#include <vector>
#include <QImage>

class SpriteManager
{
    private:
        std::map<TID, QImage> m_stoneSprites { };
        std::array<QImage, 4> m_fieldSprites { };

    public:

        void Init(QSize fsz, QSize vsz, int spriteSize, QPoint fvBeg);

        const std::map<TID, QImage>& GetAllStones() const;
        const QImage& GetStone(TID sTID) const;
        const QImage& GetBackground()    const;
        const QImage& GetCellBack()      const;
        const QImage& GetCellSelect()    const;
        const QImage& GetCellSpawn()     const;
};
