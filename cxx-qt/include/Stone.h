#pragma once

#include <memory>
#include <vector>
#include <map>

/////////////////////////////////////////////////////////////////////////////
///=- Abstract Stone definition -=///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

typedef size_t TID;
constexpr TID nullTID = 0;

class Stone
{
    friend class StoneImplementer;

    public:
        virtual size_t Score() const;
        virtual std::string GetRes() const;
        TID GetTID() const;
};

typedef std::unique_ptr<Stone> StonePtr;

/////////////////////////////////////////////////////////////////////////////
///=- Stone reflexion -=/////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

typedef StonePtr (*StoneCreator)();

class StoneImplementer final
{
public:

    static std::vector<StoneCreator>& Creators()
    {
        static std::vector<StoneCreator> m_pCreators = { };
        return m_pCreators;
    }

    template<typename Type>
    static StoneImplementer Init()
    {
        Creators().push_back([]() -> StonePtr { return std::make_unique<Type>(); });
        return StoneImplementer();
    }

    static std::map<size_t, std::string> ResourceMap()
    {
        std::map<size_t, std::string> resMap;
        for(size_t x = 0; x < Creators().size(); x++)
        {
            auto stone = Creators()[x]();
            resMap.insert({ stone->GetTID(), stone->GetRes() });
        }
        return resMap;
    }
};

#define IMPL_STONE(t) static StoneImplementer __impl_##t = StoneImplementer::Init<t>()

/////////////////////////////////////////////////////////////////////////////
///=- Concret Stone's definition -=//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class Ruby final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

class Sapphire final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

class Emerald final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

class Topaz final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

class Amethyst final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

class Alexandrite final : public Stone
{
    public:
        virtual size_t      Score()  const override;
        virtual std::string GetRes() const override;
};

/////////////////////////////////////////////////////////////////////////////
///=- Stones randomizer -=///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class StoneRandomizer
{
    public:
        std::unique_ptr<Stone> RandomStone();
};
