#pragma once

#include <memory>
#include <vector>

#include <Stone.h>

struct ivec2
{
    int x { 0 }, y { 0 };
};

enum StepType
{
    err = 0,
    l   = 1,
    r   = 2,
    u   = 3,
    d   = 4
};

StepType CharToStepType(char t);

typedef void (*TickCallback)(StonePtr&, ivec2);

/////////////////////////////////////////////////////////////////////////////
///=- FieldModel -=//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

typedef std::vector<std::vector<StonePtr>> StoneMatrix;

class FieldModel final
{
    private:
        StoneMatrix m_stones;
        size_t m_score;
        StoneRandomizer rnd;

        StonePtr& getStone(ivec2 pt);

    public:

        int width();
        int height();
        const StoneMatrix& stones() const;
        size_t score() const;

        void init(ivec2 sz, size_t seed = 0);
        bool move(ivec2 pt, StepType s);
        void mix();
        bool movesAvaliable();

        template<typename CallbackT = TickCallback>
        bool tick(CallbackT callback = [](StonePtr&, ivec2){})
        {
            auto destroy = [&](auto& s, auto coord)
            {
                m_score += s->Score();
                callback(s, coord);
                s.release();
            };

            for(int x = 0; x < m_stones.size(); x++)
            {
                for(int y = 0; y < m_stones[x].size(); y++)
                {
                    auto& s = m_stones[x][y];
                    if(!s)
                        continue;

                    auto& s1 = getStone({x - 1, y});
                    auto& s2 = getStone({x + 1, y});
                    auto& s3 = getStone({x, y - 1});
                    auto& s4 = getStone({x, y + 1});

                    if((s1 && s2) && (s->GetTID() == s1->GetTID()) &&
                                     (s->GetTID() == s2->GetTID()))
                    {
                        destroy(s,  ivec2{x,     y});
                        destroy(s1, ivec2{x - 1, y});
                        destroy(s2, ivec2{x + 1, y});
                        return true;
                    }
                    else if((s3 && s4) && (s->GetTID() == s3->GetTID()) &&
                                          (s->GetTID() == s4->GetTID()))
                    {
                        destroy(s,  ivec2{x, y    });
                        destroy(s3, ivec2{x, y - 1});
                        destroy(s4, ivec2{x, y + 1});
                        return true;
                    }
                }
            }

            /*
            struct stone { Stone* ps {nullptr}; ivec2 coord{-1, -1}; };

            auto chkStones = [](auto& s0, auto& s1, auto& s2, auto& res)
            {
                if(s1.ps && s0.ps->GetTID() == s1.ps->GetTID())
                    res.push_back(s1);
                else
                    return;

                if(s2.ps && s0.ps->GetTID() == s2.ps->GetTID())
                    res.push_back(s2);
            };

            for(int x = 0; x < m_stones.size(); x++)
            {
                for(int y = 0; y < m_stones[x].size(); y++)
                {
                    auto& s = m_stones[x][y];
                    if(!s)
                        continue;

                    std::vector<stone> vsPairs;
                    chkStones(getStone({x - 1, y}), getStone({x - 2, y}) );
                }
            }*/

            bool falls = false;
            for(size_t x = 0; x < m_stones.size(); x++)
            {
                bool cells = false;
                size_t maxEmpty = 0;
                for(size_t y = 0; y < m_stones[x].size(); y++)
                {
                    if(!m_stones[x][y])
                        maxEmpty = y;
                    else
                        cells = true;
                }
                if(!cells && maxEmpty != (m_stones[x].size() - 1))
                    continue;

                for(size_t i = maxEmpty; i > 0; i--)
                {
                    std::swap(m_stones[x][i], m_stones[x][i - 1]);
                    falls = true;
                }

                if(!m_stones[x][0] && !m_stones[x][1])
                {
                    m_stones[x][0] = rnd.RandomStone();
                    falls = true;
                }
            }

            return falls;
        }
};
